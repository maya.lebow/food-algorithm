# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 16:23:35 2021

@author: Gal Sela
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 13:29:27 2021

@author: Gal Sela
"""

### Enter required variables below: file pathway (including file name), food scoring tab, children tab and output pathways

pathway = r'C:\Users\Gal Sela\Desktop\Python\neustart\Food Algorithm Trial with raw data 71.xlsx'
food_scoring_tab = 'food content'
children_tab = 'raw data'
output_path_eats = r'C:\Users\Gal Sela\Desktop\Python\neustart\Nutrients eaten.xlsx'
output_path_nevereats = r'C:\Users\Gal Sela\Desktop\Python\neustart\Nutrients never eaten.xlsx'
output_path_avoidance = r'C:\Users\Gal Sela\Desktop\Python\neustart\Avoidance table.xlsx'



######Below is the code that should not be changed


import pandas as pd
###Mission 1 - import the two files and create 2 dataframes
def read_file (pathway, food_scoring_tab, children_tab):
    df_content= pd.read_excel(pathway,food_scoring_tab, index_col = 0)
    df_eating = pd.read_excel(pathway,children_tab)
    
    return df_content, df_eating

### call function 1
df_content, df_eating = read_file(pathway, food_scoring_tab, children_tab)

###Mission 2 - creating a translation dictionary

translate_dict = {'Egg':'ביצה','Chicken/Beef':'בשר/עוף', 'Fish':'דג', 'Nuts':'אגוזים (קשיו/פקאן/מלך', 'Seeds':'זרעי חמניה /דלעת','Olive Oil':'כף שמן זית',
'Tahini':'טחינה','Garlic/onions':'שום/בצל','Legumes':'קטניות','Oatmeal':'שיבולת שועל (או קוואקר','Broccoli/Cauliflower':'כרובית/ברוקולי',
'Tomato':'עגבנייה','Any Vegetable':'ירק אחר כלשהו','Any Fruit':'פרי כלשהו','Quinoa':'קינואה','Avocado':'אבוקדו', 'White Bread/Pita':'לחם/פיתה מקמח לבן',
'Baked Good':'מאפה קנוי (למשל רוגעלך/קרואסון/בורקס','Salty Snack':'שקית חטיף מלוח','Candy Bar':'חטיף שוקולד (למשל מקופלת/פסק זמן',
'Candy/Sweets':'סוכריה (למשל חמצוצים/טופי','milk':'חלב / גבינה לבנה /מעדן /יורגורט','butter':'חמאה','hard cheese':'גבינות מוצקות (גם צהובה',
'Orange vegetable':'גזר/בטטה/דלעת', 'Cola/ Caffinated Drinks': 'not entered','Tofu/Soy':'טופו או מוצרי סויה','Other':'אף אחד מאלו'}

### Mission 3 - creating a score dataframe for food that is eaten
score_dict_eaten = {key:{} for key in df_content.columns}
for i in df_eating.index:
    x=df_eating.iloc[i,2].replace(')','').split(', ')
    x_translated = []
    for a in x:
        for k,v in translate_dict.items():
            if v==a:
                x_translated.append(k)
            elif a not in translate_dict.values():
                print('Error: add ' + str(a) + ' to the translation dictionary')
    df_child = df_content.loc[x_translated,:].fillna(0)
    for t in df_child.columns:
        score_dict_eaten[t][i+1] = sum(df_child[t])
    score_df_eaten = pd.DataFrame(score_dict_eaten)
        
###Mission 4 - creating a score dataframe for food that is not eaten
score_dict_not = {key:{} for key in df_content.columns}
for i in df_eating.index:
    x=df_eating.iloc[i,1].replace(')','').split(', ')
    x_translated = []
    for a in x:
        for k,v in translate_dict.items():
            if v==a:
                x_translated.append(k)
            elif a not in translate_dict.values():
                print('Error: add ' + str(a) + ' to the translation dictionary')
    df_child = df_content.loc[x_translated,:].fillna(0)
    for t in df_child.columns:
        score_dict_not[t][i+1] = sum(df_child[t])*-1
    score_df_not = pd.DataFrame(score_dict_not)

med_df = score_df_eaten + score_df_not
avoidance_df = med_df.copy()
for r in med_df.index:
    for c in med_df.columns:
        if med_df.loc[r,c]==score_df_not.loc[r,c] and med_df.loc[r,c]<0:
            avoidance_df.loc[r,c] = -1
        else:
            avoidance_df.loc[r,c]=0

score_df_eaten.to_excel(output_path_eats,sheet_name ='eats')
score_df_not.to_excel(output_path_nevereats,sheet_name ='does not eat')
avoidance_df.to_excel(output_path_avoidance, sheet_name = 'avoids')